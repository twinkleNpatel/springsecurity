package com.springproject.dao;

import java.util.List;

import com.springproject.entity.UserAdmin;
import com.springproject.entity.UserRole;

public interface UserAdminDao {
		


	public List<UserAdmin> listUserAdmin();
	public void addUser(UserAdmin user);
	public UserAdmin getUserById(Integer userid);
	public List<UserRole> listUserRole();
	public void deleteUser(Integer id);
	public UserAdmin getUserByName(String userid);
	public void updateUser(UserAdmin user);

}
