package com.springproject.dao;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.springproject.entity.UserAdmin;
import com.springproject.entity.UserRole;

@Repository
public class UserAdminDaoImpl implements UserAdminDao {

	@Autowired
	private SessionFactory sessionFactory;

	@SuppressWarnings("unchecked")
	public List<UserAdmin> listUserAdmin() {
		return sessionFactory.getCurrentSession().createQuery("FROM UserAdmin")
				.list();

	}

	public void addUser(UserAdmin user) {

		UserRole roleuser = (UserRole) sessionFactory.getCurrentSession().load(UserRole.class, user.getAccess().getRoleid());
		user.setAccess(roleuser);
		sessionFactory.getCurrentSession().save(user);
	
	}

	public UserAdmin getUserById(Integer userid) {

		Session session = sessionFactory.getCurrentSession();

		Criteria criteria = session.createCriteria(UserAdmin.class);
		criteria.add(Restrictions.eq("id", userid));
		UserAdmin user = (UserAdmin) criteria.uniqueResult();
		return user;
	}

	@SuppressWarnings("unchecked")
	public List<UserRole> listUserRole() {
		
		return sessionFactory.getCurrentSession().createQuery("From UserRole")
				.list();
	}

	public void deleteUser(Integer id) {
		UserAdmin user = (UserAdmin) sessionFactory.getCurrentSession().load(UserAdmin.class, id);
	    user.setAccess(null);
		sessionFactory.getCurrentSession().delete(user);
		
	}

	public UserAdmin getUserByName(String username) {

		Session session = sessionFactory.getCurrentSession();
		Criteria criteria = session.createCriteria(UserAdmin.class);
		criteria.add(Restrictions.eq("name", username));
		UserAdmin user = (UserAdmin) criteria.uniqueResult();
		return user;

	}

	public void updateUser(UserAdmin user) {
		UserRole roleuser = (UserRole) sessionFactory.getCurrentSession().load(UserRole.class, user.getAccess().getRoleid());
		user.setAccess(roleuser);
		sessionFactory.getCurrentSession().update(user);

	}

}
