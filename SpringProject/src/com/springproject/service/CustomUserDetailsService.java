package com.springproject.service;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.GrantedAuthority;


import com.springproject.entity.UserAdmin;
import com.springproject.entity.UserRole;


public class CustomUserDetailsService<grantedauthority> implements
		UserDetailsService {

	@Autowired
	private UserAdminService userAdminServiceImpl;
	
	
	UserDetails user = null;

	public UserDetails loadUserByUsername(String username)
			throws UsernameNotFoundException {
		
		System.out.println("CustomUserDetailsService"+username);
		try{
		UserAdmin userDb =  userAdminServiceImpl.getUserByName(username);
	    System.out.println("userdb pass"+ userDb.getPassword());
	   // Md4PasswordEncoder encoder = new Md4PasswordEncoder();
	  // System.out.println(encoder.encodePassword(userDb.getPassword(), "1"));
	    
		user =  new User(userDb.getName(), userDb.getPassword(), true, true, true, true, getAuthorities(userDb.getAccess().getRoleid()));
		}
		
		catch(Exception e)
		{
			System.out.println("user not found");
		}
		return user;
	}

	
	
	public Collection<GrantedAuthority> getAuthorities(Integer userRole) {
	    List<GrantedAuthority> authList = new ArrayList<GrantedAuthority>(2);

	    authList.add(new SimpleGrantedAuthority("ROLE_USER"));
	    
	    if (userRole == 1) {
	        authList.add(new SimpleGrantedAuthority("ROLE_ADMIN"));
	    }
	   
	    return authList;
	}

}
