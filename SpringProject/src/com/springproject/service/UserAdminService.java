package com.springproject.service;

import java.util.List;

import com.springproject.entity.UserAdmin;
import com.springproject.entity.UserRole;

public interface UserAdminService {
	
	public List<UserAdmin> listUserAdmin();
	public void addUser(UserAdmin user);
    public UserAdmin getUserById(Integer userid);
    public List<UserRole> listUserRole();
    public void deleteUser(Integer id);
    public UserAdmin getUserByName(String username); 
    public void updateUser(UserAdmin user);
}
