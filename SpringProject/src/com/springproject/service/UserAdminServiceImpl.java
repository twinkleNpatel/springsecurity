package com.springproject.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.encoding.ShaPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.springproject.dao.UserAdminDao;
import com.springproject.entity.UserAdmin;
import com.springproject.entity.UserRole;

@Service
public class UserAdminServiceImpl implements UserAdminService {

	@Autowired
	UserAdminDao userAdminDaoImpl;

	@Transactional
	public List<UserAdmin> listUserAdmin() {
		return userAdminDaoImpl.listUserAdmin();
	}

	@Transactional
	public void addUser(UserAdmin user) {
		ShaPasswordEncoder encoder = new ShaPasswordEncoder();
		user.setPassword((encoder.encodePassword(user.getPassword(), "")));
		userAdminDaoImpl.addUser(user);
	}

	@Transactional
	public List<UserRole> listUserRole() {
		return userAdminDaoImpl.listUserRole();
	}

	@Transactional
	public void deleteUser(Integer id) {
		userAdminDaoImpl.deleteUser(id);
	}

	@Transactional
	public UserAdmin getUserById(Integer userid) {
		UserAdmin user  = userAdminDaoImpl.getUserById(userid);
		 return user;
	}

	@Transactional
	public UserAdmin getUserByName(String username) {
		return userAdminDaoImpl.getUserByName(username);
	}

	@Transactional
	public void updateUser(UserAdmin user) {
		userAdminDaoImpl.updateUser(user);

	}

}
