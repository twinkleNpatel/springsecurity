package com.springproject.entity;

import javax.persistence.*;
import java.lang.Integer;

@Entity
@Table(name = "user")
public class UserAdmin {

	@Id
	@GeneratedValue
	@Column(name = "userid")
	private Integer id;

	@Column(name = "username")
	private String name;

	@Column(name = "userpassword")
	private String password;
	
	@ManyToOne(cascade=CascadeType.ALL)
	@JoinColumn(name="userrole")
	private UserRole access;
	
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	
	public UserRole getAccess() {
		return access;
	}

	
	public void setAccess(UserRole access) {
		this.access = access;
	}

	

	public UserAdmin() {
		super();
		// TODO Auto-generated constructor stub
	}

}