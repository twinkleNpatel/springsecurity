package com.springproject.entity;

import java.util.Set;

import javax.persistence.*;

@Entity
@Table(name="role")
public class UserRole {

	@Id
	@GeneratedValue
	@Column(name="roleid")
	/*@OneToMany(mappedBy="role")*/
	private Integer roleid;

	@Column(name="rolename", unique=true)
	private String rolename;

	


	public Integer getRoleid() {
		return roleid;
	}

	public void setRoleid(Integer roleid) {
		this.roleid = roleid;
	}

	public String getRolename() {
		return rolename;
	}

	public void setRolename(String rolename) {
		this.rolename = rolename;
	}

    
}
