package com.springproject.controller;

import java.util.Collection;

import java.util.Map;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;

import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.springproject.entity.UserAdmin;
import com.springproject.service.UserAdminService;

@Controller
public class SpringSecurityController {

	@Autowired
	private UserAdminService useradminservice;

	@RequestMapping(value = "/index")
	public String newUser(Map<String, Object> map) {
		map.put("user", new UserAdmin());
		map.put("useradminlist", useradminservice.listUserAdmin());
		map.put("rolelist", useradminservice.listUserRole());
		return "admin";
	}

	@RequestMapping(value = "/login")
	public String getLoginPage(
			@RequestParam(value = "error", required = false) boolean error,
			ModelMap model) {

		if (error == true) {
			// Assign an error message
			model.put("error",
					"You have entered an invalid username or password!");
		} else {
			model.put("error", "");
		}

		return "loginpage";
	}

	@RequestMapping(value = "/denied", method = RequestMethod.GET)
	public String getDeniedPage() {

		return "deniedpage";
	}

	@RequestMapping(value = "/redirect", method = RequestMethod.GET)
	public String getCommanPage() {

		Object principal = SecurityContextHolder.getContext()
				.getAuthentication().getPrincipal();

		if (principal instanceof UserDetails) {
			Collection<? extends GrantedAuthority> access = ((UserDetails) principal)
					.getAuthorities();
			Integer level = access.toArray().length;
			if (level == 2) {
				return "redirect: /index";
			} else {
				return "common";
			}

		}

		return "loginpage";
	}

	@RequestMapping(value = "/add", method = RequestMethod.POST)
	public String adduser(@ModelAttribute("user") UserAdmin user) {
		
		if(user.getId()==null){
		useradminservice.addUser(user);
		}
		else{
			useradminservice.updateUser(user);
			System.out.println("updated");
		}
		return "redirect: /index";

	}

	@RequestMapping("/delete/{id}")
	public String deleteUser(@PathVariable("id") Integer userid) {
		
		useradminservice.deleteUser(userid);
		return "redirect:/index";

	}

	@RequestMapping(value = "/Edit/{id}")
	public String editUser(@PathVariable("id") Integer userid,
			Map<String, Object> map) {
		map.put("user", useradminservice.getUserById(userid));
		map.put("userList", useradminservice.listUserAdmin());
		map.put("rolelist", useradminservice.listUserRole());
		map.put("useradminlist", useradminservice.listUserAdmin());
		return "admin";
	}

}