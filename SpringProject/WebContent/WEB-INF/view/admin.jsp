<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Insert title here</title>
</head>
<body>

	<h1>welcome Admin</h1>
	<br>
	<h4>Add user Here</h4>
	<c:url var="action" value="/add"></c:url>
	<form:form method="post" action="${action}" commandName="user">
		<table>

			<tr>
				<td><form:hidden path="id" /></td>
			</tr>

			<tr>
				<td><form:label path="name">Name</form:label></td>
				<td><form:input type="text" path="name" /></td>
			</tr>


			<tr>
				<td><form:label path="password">Password</form:label></td>
				<td><form:input type="password" path="password"></form:input></td>
			</tr>



		<%-- 	<tr>
				<td><form:label path="access.rolename">Role</form:label></td>
				<td><form:input type="text" path="access.rolename"></form:input></td>
			</tr> --%>

			<tr>
				<td><form:select path="access.roleid">Role
			  <c:choose>
							<c:when test="${ empty rolelist}">
								<form:option value="" label="Add any role first" disabled="yes" />
							</c:when>
							<c:otherwise>
								<form:options items="${rolelist}" itemLabel="rolename"  itemValue="roleid"/>
							</c:otherwise>
			  </c:choose>

					</form:select></td>
			</tr> 

			<tr>
				<td><input type="submit" value="submit"></td>
			</tr>
		</table>

	</form:form>
    <br>
    <br>
    <h1>List of Users</h1> 
<c:if test="${empty useradminlist}"></c:if>
<table>
<c:forEach items="${useradminlist}" var="user">

			<tr>
				<td>${user.name}</td>
				<td>${user.password}</td>
				<td>${user.access.rolename}</td>
				<td><a href="<c:url value='/Edit/${user.id}'/>">edit</a></td>
				<td><a href="<c:url value='/delete/${user.id}'/>">delete</a></td>
			</tr>

		</c:forEach>
</table>
	<br>
	<a href="<%=request.getContextPath()%>/j_spring_security_logout">LogOut</a>

</body>
</html>